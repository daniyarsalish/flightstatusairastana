﻿using Domain.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public interface IGenericRepository<T, M> where T : class where M : BaseModel
    {
        Task<IList<M>> GetAllAsync();
        Task<M?> GetAsync(int id);
        Task AddAsync(M model);
        Task UpdateAsync(int id, M model);
        Task DeleteAsync(int id);
    }
}
