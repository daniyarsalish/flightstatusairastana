﻿using Domain.Common;
using Domain.Interfaces;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using Infrastructure.Interfaces;
using Domain.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Serilog;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using StackExchange.Redis;
using Newtonsoft.Json;

namespace Infrastructure.Repos
{
    public class GenericRepository<T, M> : IGenericRepository<T, M> where T : class where M : BaseModel
    {
        private readonly DbContext _dataContext;
        private readonly Mapper _mapper;
        private readonly IAutomapperConfig _config;
        private readonly ILogger _logger;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IDatabase _cache;

        public GenericRepository(DbContext dataContext, IAutomapperConfig config, ILogger logger, IHttpContextAccessor? httpContextAccessor, IConnectionMultiplexer redis)
        {
            _dataContext = dataContext;
            _config = config;
            _mapper = _config.InitializeAutomapper();
            _logger = logger;
            _httpContextAccessor = httpContextAccessor;
            _cache = redis.GetDatabase();
        }

        public async Task AddAsync(M model)
        {
            T entity = _mapper.Map<T>(model);
            await _dataContext.Set<T>().AddAsync(entity);
            await _dataContext.SaveChangesAsync();
            _cache.KeyDelete(typeof(T).FullName);
            var username = _httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            _logger.Information("User {User} added a new entity at {Time}", username, DateTime.Now);
        }

        public async Task DeleteAsync(int id)
        {
            T entity = await _dataContext.Set<T>().FindAsync(id);
            if (entity != null)
            {
                _dataContext.Set<T>().Remove(entity);
                await _dataContext.SaveChangesAsync();
                _cache.KeyDelete(typeof(T).FullName); 
                var username = _httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
                _logger.Information("User {User} deleted entity {id} at {Time}", username, id.ToString(), DateTime.Now);
            }
        }

        public async Task<IList<M>> GetAllAsync()
        {

            List<T> values;
            var cacheKey = typeof(T).FullName;
            var cachedValues = _cache.StringGet(cacheKey);
            if (!cachedValues.HasValue)
            {
                values = await _dataContext.Set<T>().ToListAsync();
                _cache.StringSet(cacheKey, JsonConvert.SerializeObject(values)); 
            }
            else
            {
                values = JsonConvert.DeserializeObject<List<T>>(cachedValues); 
            }
            var models = new List<M>();
            foreach (var entity in values)
            {
                models.Add(_mapper.Map<M>(entity));
            }
            return models;
        }

        public async Task<M?> GetAsync(int id)
        {
            return _mapper.Map<M>(await _dataContext.Set<T>().FindAsync(id));
        }

        public async Task UpdateAsync(int id, M model)
        {
            T exist = await _dataContext.Set<T>().FindAsync(id);
            if (exist != null)
            {
                var properties = typeof(M).GetProperties().Where(p => p.Name != "Id");

                foreach (var property in properties)
                {
                    var value = property.GetValue(model);
                    typeof(T).GetProperty(property.Name).SetValue(exist, value);
                }

                await _dataContext.SaveChangesAsync();
                _cache.KeyDelete(typeof(T).FullName);
                var username = _httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
                _logger.Information("User {User} updated entity {id} at {Time}", username, id.ToString(), DateTime.Now);
            }
        }


    }
}
