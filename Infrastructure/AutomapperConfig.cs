﻿using AutoMapper;
using Domain.Entities;
using Domain.Models;
using Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure
{
    public class AutomapperConfig : IAutomapperConfig
    {
        public Mapper InitializeAutomapper()
        {
            //Provide all the Mapping Configuration
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<User, UserModel>();
                cfg.CreateMap<UserModel, User>()
                .ForMember(dest => dest.PasswordHash, act => act.Ignore())
                .ForMember(dest => dest.Salt, act => act.Ignore())
                .AfterMap((src, dest) => dest.SetPassword(src.Password))
                .ForMember(dest => dest.Id, opt => opt.Ignore());
                ;
                cfg.CreateMap<Role, RoleModel>();
                cfg.CreateMap<RoleModel, Role>()
                .ForMember(dest => dest.Id, opt => opt.Ignore()); ;
                cfg.CreateMap<Flight, FlightModel>();
                cfg.CreateMap<FlightModel, Flight>()
                .ForMember(dest => dest.Id, opt => opt.Ignore()); ;
            });
            var mapper = new Mapper(config);
            return mapper;
        }
    }
}
