﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Enums
{
    public enum FlightStatus
    {
        none = 0,
        InTime = 1,
        Delayed = 2,
        Cancelled = 3
    }
}
