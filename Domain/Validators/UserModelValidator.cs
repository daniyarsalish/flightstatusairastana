﻿using Castle.Core.Resource;
using Domain.Entities;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Models;

namespace Domain.Validators
{
    public class UserModelValidator : AbstractValidator<UserModel>
    {
        public UserModelValidator()
        {
            RuleFor(x => x.Username).NotEmpty().WithMessage("Please enter your username");
            RuleFor(x => x.Password).Length(6, 250).WithMessage("Please enter a password of 6 characters or more");
        }
    }
    
}
