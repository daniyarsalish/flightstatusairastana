﻿using Domain.Models;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Validators
{
    public class FlightModelValidator : AbstractValidator<FlightModel>
    {
        public FlightModelValidator()
        {
            RuleFor(x => x.Origin).Length(3)
                .WithMessage("The input must contain exactly 3 characters.")
                .Matches("^[a-zA-Z]*$")
                .WithMessage("The input must contain only Latin characters.");
            RuleFor(x => x.Destination).Length(3)
                .WithMessage("The input must contain only Latin characters.")
                .Matches("^[a-zA-Z]*$")
                .WithMessage("The input must contain only Latin characters.");
            RuleFor(x => x.Status)
                .IsInEnum()
                .WithMessage("The status must be one of the FlightStatus enum values.");
        }
    }
}
