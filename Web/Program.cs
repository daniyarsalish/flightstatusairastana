﻿using Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Domain.Interfaces;
using Infrastructure.Repos;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Umbraco.Core.Services;
using Infrastructure.Interfaces;
using Infrastructure;
using Microsoft.AspNetCore.Identity;
using Domain.Entities;
using System.Security.Claims;
using FluentValidation;
using Domain.Models;
using Domain.Validators;
using Serilog.Events;
using Serilog;
using StackExchange.Redis;

var builder = WebApplication.CreateBuilder(args);

// Настройка Serilog
Log.Logger = new LoggerConfiguration()
    .MinimumLevel.Debug()
    .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
    .Enrich.FromLogContext()
    .WriteTo.File("C:\\Users\\User\\Desktop\\FSAA.txt", rollingInterval: RollingInterval.Hour)
    .CreateLogger();
    builder.Services.AddSingleton(Log.Logger);
try
{
    Log.Information("Starting up");
    // Добавление сервисов в контейнер
    builder.Services.AddControllers();
    // Узнайте больше о настройке Swagger/OpenAPI по адресу https://aka.ms/aspnetcore/swashbuckle
    builder.Services.AddEndpointsApiExplorer();
    builder.Services.AddSwaggerGen();
    builder.Services.AddSingleton<IConnectionMultiplexer>(sp =>
    {
        var configuration = ConfigurationOptions.Parse(builder.Configuration.GetConnectionString("RedisConnection"), true);
        return ConnectionMultiplexer.Connect(configuration);
    });

    builder.Services.AddDbContext<DataContext>(x =>
    {
        x.UseNpgsql(builder.Configuration.GetConnectionString("db"), b => b.MigrationsAssembly("Web"));
        x.UseSnakeCaseNamingConvention();
    });
    builder.Services.AddScoped<IAutomapperConfig, AutomapperConfig>();
    builder.Services.AddScoped(typeof(DbContext), typeof(DataContext));
    builder.Services.AddScoped(typeof(IGenericRepository<,>), typeof(GenericRepository<,>));
    builder.Services.AddScoped<IValidator<UserModel>, UserModelValidator>();
    builder.Services.AddScoped<IValidator<FlightModel>, FlightModelValidator>();
    builder.Services.AddHttpContextAccessor();
    builder.Services.AddAuthorization();
    builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
        .AddJwtBearer(options =>
        {
            options.TokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidIssuer = builder.Configuration["JwtSettings:Issuer"],
                ValidateAudience = true,
                ValidAudience = builder.Configuration["JwtSettings:Audience"],
                ValidateLifetime = true,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(builder.Configuration["JwtSettings:SecretKey"])),
                ValidateIssuerSigningKey = true,
            };
        });
    builder.Services.AddAuthorization(options =>
    {
        options.AddPolicy("admin", policy => policy.RequireClaim(ClaimTypes.Role, "admin"));
        options.AddPolicy("user", policy => policy.RequireClaim(ClaimTypes.Role, "user"));
    });

    var app = builder.Build();
    app.MapControllers();

    if (app.Environment.IsDevelopment())
    {
        app.UseSwagger();
        app.UseSwaggerUI();
    }

    app.UseHttpsRedirection();
    app.UseAuthentication();
    app.UseStaticFiles();
    app.UseRouting();
    app.UseAuthorization();
    app.UseEndpoints(endpoints =>
    {
        endpoints.MapControllers();
    });

    app.Run();
}
catch (Exception ex)
{
    Log.Fatal(ex, "Application start-up failed");
}
finally
{
    Log.CloseAndFlush();
}
