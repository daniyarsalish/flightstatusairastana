﻿using Domain.Entities;
using Domain.Interfaces;
using Domain.Models;
using FluentValidation;
using Infrastructure.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IGenericRepository<User, UserModel> _userRepository;
        private readonly IValidator<UserModel> _validator;
        public UserController(IValidator<UserModel> validator, IGenericRepository<User, UserModel> userRepository)
        {
            _userRepository = userRepository;
            _validator = validator;
        }

        // GET: api/<UserController>
        [HttpGet]
        [Authorize]
        public async Task<ActionResult<IList<UserModel>>> GetAll()
        {
            return Ok(await _userRepository.GetAllAsync());
        }

        // GET api/<UserController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<UserModel?>> Get(int id)
        {
            return Ok(await _userRepository.GetAsync(id));
        }

        // POST api/<UserController>
        [HttpPost]
        [Authorize(Policy = "admin")]
        public async Task<ActionResult> Add([FromBody] UserModel model)
        {
            var validationResult = _validator.Validate(model);
            if (!validationResult.IsValid)
            {
                return BadRequest(validationResult.Errors);
            }
            await _userRepository.AddAsync(model);
            return Ok();
        }

        // PUT api/<UserController>/5
        [HttpPut("{id}")]
        [Authorize(Policy = "admin")]
        public async Task<IActionResult> Put(int id, [FromBody] UserModel model)
        {
            var validationResult = _validator.Validate(model);

            if (!validationResult.IsValid)
            {
                return BadRequest(validationResult.Errors);
            }
            var role = await _userRepository.GetAsync(id);
            if (role == null)
            {
                return BadRequest("User not found");
            }
            await _userRepository.UpdateAsync(id, model);
            return Ok();
        }

        // DELETE api/<UserController>/5
        [HttpDelete("{id}")]
        [Authorize(Policy = "admin")]
        public async Task<IActionResult> Delete(int id)
        {
            var flight = await _userRepository.GetAsync(id);
            if (flight == null)
            {
                return BadRequest("User not found");
            }
            await _userRepository.DeleteAsync(id);
            return Ok();
        }
    }
}
