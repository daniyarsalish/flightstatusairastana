﻿using Castle.Core.Resource;
using Domain.Interfaces;
using Domain.Models;
using FluentValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Data;
using System.Security.Claims;

namespace Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FlightController : ControllerBase
    {
        private readonly IGenericRepository<Flight, FlightModel> _flightRepository;
        private readonly IValidator<FlightModel> _validator;
        public FlightController(IValidator<FlightModel> validator, IGenericRepository<Flight, FlightModel> flightRepository)
        {
            _flightRepository = flightRepository;
            _validator = validator;
        }

        // GET: api/<FlightController>
        [HttpGet]
        public async Task<ActionResult<IList<FlightModel>>> GetAll()
        {
            return Ok(await _flightRepository.GetAllAsync());
        }

        // GET api/<FlightController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Flight?>> Get(int id)
        {
            return Ok(await _flightRepository.GetAsync(id));
        }

        // POST api/<FlightController>
        [HttpPost]
        [Authorize(Policy = "admin")]
        public async Task<ActionResult> Add([FromBody] FlightModel model)
        {
            var validationResult = _validator.Validate(model);
            if (!validationResult.IsValid)
            {
                return BadRequest(validationResult.Errors);
            }
            await _flightRepository.AddAsync(model);
            return Ok();
        }

        // PUT api/<FlightController>/5
        [HttpPut("{id}")]
        [Authorize(Policy = "admin")]
        public async Task<IActionResult> Put(int id, [FromBody] FlightModel model)
        {
            var validationResult = _validator.Validate(model);
            if (!validationResult.IsValid)
            {
                return BadRequest(validationResult.Errors);
            }
            var role = await _flightRepository.GetAsync(id);
            if (role == null)
            {
                return BadRequest("Flight not found");
            }
            await _flightRepository.UpdateAsync(id, model);
            return Ok();
        }

        // DELETE api/<FlightController>/5
        [HttpDelete("{id}")]
        [Authorize(Policy = "admin")]
        public async Task<IActionResult> Delete(int id)
        {
            var flight = await _flightRepository.GetAsync(id);
            if (flight == null)
            {
                return BadRequest("Flight not found");
            }
            await _flightRepository.DeleteAsync(id);
            return Ok();
        }
    }
}
