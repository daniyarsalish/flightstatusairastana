﻿using Domain.Entities;
using Domain.Interfaces;
using Domain.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

namespace Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoleController : ControllerBase
    {
        private readonly IGenericRepository<Role, RoleModel> _roleRepository;
        public RoleController(IGenericRepository<Role, RoleModel> roleRepository)
        {
            _roleRepository = roleRepository;
        }

        // GET: api/<RoleController>
        [HttpGet]
        public async Task<ActionResult<IList<RoleModel>>> GetAll()
        {
            return Ok(await _roleRepository.GetAllAsync());
        }

        // GET api/<RoleController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Role?>> Get(int id)
        {
            return Ok(await _roleRepository.GetAsync(id));
        }

        // POST api/<RoleController>
        [HttpPost]
        [Authorize(Policy = "admin")]
        public async Task<ActionResult> Add([FromBody] RoleModel role)
        {
            await _roleRepository.AddAsync(role);
            return Ok();
        }

        // PUT api/<RoleController>/5
        [HttpPut("{id}")]
        [Authorize(Policy = "admin")]
        public async Task<IActionResult> Put(int id, [FromBody] RoleModel model)
        {
            var role = await _roleRepository.GetAsync(id);
            if (role == null)
            {
                return BadRequest("Role not found");
            }
            await _roleRepository.UpdateAsync(id, model);
            return Ok();
        }

        // DELETE api/<RoleController>/5
        [HttpDelete("{id}")]
        [Authorize(Policy = "admin")]
        public async Task<IActionResult> Delete(int id)
        {
            var role = await _roleRepository.GetAsync(id);
            if (role == null)
            {
                return BadRequest("Role not found");
            }
            await _roleRepository.DeleteAsync(id);
            return Ok();
        }
    }
}
